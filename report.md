# Semestral work report for the NI-MVI class

## Assignment description

Enhance old games frame rate to give old games more modern feel. Do this by creating Unet and GAN neural networks. Compare generated frames
and show some slow motion footage from that games.

## Data Preparation

In this semestral work I am going to enhance framerate of old games for Nintendo Entertainment System (NES). I am going to use Unet and GAN architecture and compare results.

All the data for my dataset was scrapped from [https://nesguide.com/genre/shooter](NesGuide) using `youtube-dl`. Then I used `ffmpeg` to cut desired part of the video like this.

```
ffmpeg -ss 00:03:07 -i batman.mp4 -to 00:00:10 -c copy batman_cut.mp4
```

Then I created [data generator](https://www.tensorflow.org/api_docs/python/tf/data/Dataset#from_generator) using TensorFlow capabilities, which can be found in the jupyter notebook.

## Papers

My research journey started with the documents I was given in `MI-MVI ZS 2021` document.

https://www.citi.sinica.edu.tw/papers/yylin/6497-F.pdf
http://tedxiao.me/pdf/CS294_Report.pdf
https://github.com/neil454/deep-motion

Additionally I found this article about frame enhancement using GANs, which was the foundation for my GAN implementation.

https://www.sciencedirect.com/science/article/abs/pii/S0925231219315747

For the Unet model I went with [deep-motion](https://github.com/neil454/deep-motion)

## Models

In this section I am going to quickly describe models used in this semestral work.

### Normalization

Images were resized to (360, 240) and pixel values normalized between `0-1`.

### Unet Model

Consists of 3 sections. Down-sampling, bottleneck and up-sampling sections. Down-sampling section typically consists of Convolution layer, BatchNormalization layer, LeakyRelu and MaxPooling. First down-sampling part doesn't have batch normalization. 

Up-sampling layer does symmetrically opposite of what down-sampling layer does. It consists of UpSampling, to match dimensions with the symmetrically opposite down-sampling layer. Then we concatenate down and up sampling parts and result pass to convolution layers.

It takes an Input in a form of `(img_height, img_width, 6)`. Where `6` is frame channels of X_1 and X_3 frames concatenated. Output of the given model is frame X_2 in the form of `(img_heigh, img_width, 3)`.

Model was compiled with `adam` optimalizator without hyper-parameter tunning (used default values) and as a loss function I picked `mse`.

This model is located in `unet_model.py` file.

### GAN

GAN is an model where 2 competing models play a game. Generator is trying to generate from random noise an image, signal, etc. so that Discriminator can't tell the difference between the real one and the generated one. Discriminator is trying to guess if it's generated or not. They are playing zero sum game, so if 1 wins second loses, and his weights are going to be updated.

In my case, generator takes 2 images X_1 and X_3 and tries to generate X_2 in a way that discriminator can't recognize if its real or not. Generator model is tweaked Unet model from the second model from this semestral work.

Discriminator is just simple deep convolutional model.

I use `adam` optimalizator for this model also, with tweeked parameters. For loss function I have chosen binary cross entropy and override default `model.fit` logic to make training code use discriminator / generator mechanic.

Additionally I added accuracy metric called structural similarity to get best weights for the generator to be saved. This way generator is not completely unsupervised.

Because I normalized frames between 0 and 1, I needed generator activation function in the last layer to output values in that range also. For that reason I used `sigmoid` function. If I would normalize frames for example between -1 and 1, then I would need to use `tanh` or other activation function that outputs values in that range.

This model is located in `gan_model.py` file.

### Usage

Notebook that's inside this repository can be used to generated higher framerate version of videos from [https://nesguide.com/genre/shooter](NesGuide).

Weights are available in the weights folder. Retraining can be enabled using `TRAIN_GAN` and `TRAIN_UNET` constants in the jupyter notebook.

### Results

I managed to generate believable frames from both models. Examples are found in the notebook.

### Conclusion

I created 2 models and compared their generated frames inside the jupyter notebook. Additionally I wrote simple script in it to generate higher frequency videos for old video games.

### What to do next

I could tune hyper-parameters of the GAN model more and also let the learning run for a longer period of time.