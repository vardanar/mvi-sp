import keras
import keras.layers as layers  # pylance please
from keras.models import Model
import keras.metrics as metrics
import tensorflow as tf
import tensorflow.image as image

img_width = 320
img_height = 256


def discriminator():
    def conv(x, y, z, inp):
        _ = layers.Conv2D(x, (y, z), strides=(2, 2), padding="same")(inp)
        _ = layers.BatchNormalization()(_)
        return layers.LeakyReLU(alpha=0.2)(_)

    inputs = layers.Input((img_height, img_width, 3))

    conv1 = layers.Conv2D(16, (4, 4), strides=(2, 2), padding="same")(inputs)
    lrelu0 = layers.LeakyReLU(alpha=0.2)(conv1)

    conv2 = conv(32, 4, 4, lrelu0)
    conv3 = conv(64, 4, 4, conv2)
    conv4 = conv(128, 4, 4, conv3)
    conv5 = conv(256, 4, 4, conv4)
    conv6 = conv(512, 4, 4, conv5)

    flat = layers.Flatten()(conv6)

    dens1 = layers.Dense(512)(flat)
    lrelu1 = layers.LeakyReLU(alpha=0.2)(flat)
    dens2 = layers.Dense(512)(lrelu1)
    lrelu2 = layers.LeakyReLU(alpha=0.2)(dens2)
    dens3 = layers.Dense(256)(lrelu2)
    lrelu2 = layers.LeakyReLU(alpha=0.2)(dens3)

    drop = layers.Dropout(0.2)(lrelu2)

    output = layers.Dense(1, activation="sigmoid")(drop)

    return Model(inputs, output)


def unet_conv(x, y, z, to):
    conv = layers.Conv2D(x, (y, z), padding="same")(to)
    relu = layers.LeakyReLU()(conv)
    return layers.BatchNormalization()(relu)


def unet_downconv(x, y, z, to):
    c = unet_conv(x, y, z, to)
    c = unet_conv(x, y, z, c)
    return c, layers.MaxPooling2D(pool_size=(2, 2), padding="same", strides=(2, 2))(c)


def unet_upconv(x, y, z, to1, to2):
    upsam = layers.UpSampling2D((2, 2))(to1)
    m1 = layers.Concatenate(axis=-1)([upsam, to2])
    c = unet_conv(x, y, z, m1)
    return unet_conv(x, y, z, c)


def generator(activation="sigmoid"):
    inputs = layers.Input(shape=(img_height, img_width, 6))

    # conv1, pool1 = unet_downconv(32, 3, 3, inputs)
    conv1 = layers.Conv2D(32, (3, 3), padding="same")(inputs)
    relu = layers.LeakyReLU()(conv1)
    pool1 = layers.MaxPooling2D(pool_size=(2, 2), padding="same", strides=(2, 2))(relu)

    conv2, pool2 = unet_downconv(64, 3, 3, pool1)
    conv3, pool3 = unet_downconv(128, 3, 3, pool2)
    conv4, pool4 = unet_downconv(256, 3, 3, pool3)

    conv5 = unet_conv(512, 3, 3, pool4)
    conv5 = unet_conv(512, 3, 3, conv5)

    conv6 = unet_upconv(256, 3, 3, conv5, conv4)
    conv7 = unet_upconv(128, 3, 3, conv6, conv3)
    conv8 = unet_upconv(64, 3, 3, conv7, conv2)
    conv9 = unet_upconv(32, 3, 3, conv8, conv1)

    conv10 = layers.Conv2D(3, (1, 1), activation=activation, padding="same")(conv9)

    return Model(inputs, conv10)


# def create_disc_input(X, new):
#   firsts, thirds = np.split(X, 2, axis=-1)

#   res = np.concatenate([firsts, new, thirds], axis=-1)
#   print(res.shape)
#   return res


class FrameGAN(keras.Model):
    def __init__(self, gen, disc):
        super(FrameGAN, self).__init__()
        self.disc = disc
        self.gen = gen
        self.gen_loss_tracker = metrics.Mean(name="gen_loss")
        self.disc_loss_tracker = metrics.Mean(name="disc_loss")
        self.ssim_accuracy = metrics.Mean(name="ssim_accuracy")

    @property
    def metrics(self):
        return [self.gen_loss_tracker, self.disc_loss_tracker, self.ssim_accuracy]

    def compile(self, disc_opt, gen_opt, loss_fn):
        super(FrameGAN, self).compile()
        self.disc_opt = disc_opt
        self.gen_opt = gen_opt
        self.loss_fn = loss_fn

    def predict(
        self,
        x,
        batch_size=None,
        verbose=0,
        steps=None,
        callbacks=None,
        max_queue_size=10,
        workers=1,
        use_multiprocessing=False,
    ):
        return self.gen.predict(
            x,
            batch_size,
            verbose,
            steps,
            callbacks,
            max_queue_size,
            workers,
            use_multiprocessing,
        )

    def train_step(self, images):
        X, y = images

        batch_size = tf.shape(X)[0]

        generated_images = self.gen(X)

        y = tf.cast(y, tf.float32)

        combined = tf.concat([generated_images, y], axis=0)

        labels = tf.concat(
            [tf.zeros((batch_size, 1)), tf.ones((batch_size, 1))], axis=0
        )

        # make noisy labels!
        labels += 0.05 * tf.random.uniform(tf.shape(labels))

        # train the discriminator
        with tf.GradientTape() as tape:
            predictions = self.disc(combined, training=True)
            d_loss = self.loss_fn(labels, predictions)
            d_loss /= 2  # make discriminator loss function smaller

        grads = tape.gradient(d_loss, self.disc.trainable_weights)
        self.disc_opt.apply_gradients(zip(grads, self.disc.trainable_weights))

        misleading_labels = tf.ones((batch_size, 1))

        # train the generator
        with tf.GradientTape() as tape:
            # fake_input = create_disc_input(X, preds_g)
            preds = self.disc(self.gen(X, training=True), training=False)
            g_loss = self.loss_fn(misleading_labels, preds)

        grads = tape.gradient(g_loss, self.gen.trainable_weights)
        self.gen_opt.apply_gradients(zip(grads, self.gen.trainable_weights))
        self.gen_loss_tracker.update_state(g_loss)
        self.disc_loss_tracker.update_state(d_loss)
        self.ssim_accuracy.update_state(ssim(self.gen(X), y, 1))

        return {
            "gen_loss": self.gen_loss_tracker.result(),
            "disc_loss": self.disc_loss_tracker.result(),
            "ssim_accuracy": self.ssim_accuracy.result(),
        }

